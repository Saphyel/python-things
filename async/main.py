import asyncio
import logging
from types import coroutine
import httpx
import time

urls = ["https://www.google.com", "https://www.reddit.com", "https://www.x.com", "https://www.microsoft.com/"]

logging.basicConfig(level=logging.INFO)
logging.getLogger("httpx").setLevel(logging.INFO)


async def future_get(client: httpx.AsyncClient, url: str) -> coroutine:
    return client.get(url)


async def await_get(client: httpx.AsyncClient, url: str) -> httpx.Response:
    return await client.get(url)


async def future_main():
    client = httpx.AsyncClient()
    result = await asyncio.gather(*[await future_get(client, item) for item in urls])
    print(result)
    await client.aclose()


async def await_main():
    client = httpx.AsyncClient()
    print([await await_get(client, item) for item in urls])
    await client.aclose()


async def hybrid_main():
    gatherio = []
    client = httpx.AsyncClient()
    for item in urls:
        get = await future_get(client, item)
        response = await get

        gatherio.append(response)
    print(gatherio)
    await client.aclose()


start = time.process_time()
asyncio.run(future_main())
print(f":: FUTURE WAY TOOK {time.process_time() - start} ::", *urls)


start = time.process_time()
asyncio.run(await_main())
print(f":: AWAIT WAY TOOK {time.process_time() - start} ::", *urls)


start = time.process_time()
asyncio.run(hybrid_main())
print(f":: HYBRID WAY TOOK {time.process_time() - start} ::", *urls)
