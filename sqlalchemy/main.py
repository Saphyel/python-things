import asyncio
import httpx

async def demon(url: str | None = None):
    print(f'Start {url}')
    httpx.get(url)
    print(f'End {url}')

async def main():
    await asyncio.gather(demon(), demon())

asyncio.run(main())
